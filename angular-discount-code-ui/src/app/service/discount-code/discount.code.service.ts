import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { DiscountCode } from 'src/app/models/discountCode.model';
import { Observable } from 'rxjs';
import { SpecialOffer } from 'src/app/models/specialoffer.model';

@Injectable()
export class DiscountCodeService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  apiURL: string = 'http://localhost:8080/api/v1/discountCode';
  percenatgeURL: string = 'http://localhost:8080/api/v1/discountCode';

  constructor(private httpClient: HttpClient) { }

  public generateDiscountCodes(specialOffer: SpecialOffer, expirationDate: string): Observable<DiscountCode[]> {
    return this.httpClient.post<DiscountCode[]>(this.apiURL + '/' + expirationDate + '/' + 'generate', specialOffer, this.httpOptions);
  }

  // if dicountCode is valid then set usage date
  public getPercentageDiscount(email: string, discountCode: string): Observable<number> {
    return this.httpClient.put<number>(this.apiURL + '/' + email + '/' + discountCode + '/' + 'get', null, this.httpOptions);
  }

  public getValidDiscountsForEmail(email: string): Observable<Map<string, string>> {
    return this.httpClient.get<Map<string, string>>(this.apiURL + '/' + email + '/' + 'get');
  }

}
