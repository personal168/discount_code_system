import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SpecialOffer } from 'src/app/models/specialoffer.model';

@Injectable()
export class SpecialOfferService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  apiURL: string = 'http://localhost:8080/api/v1/discountCode/specialOffer';
  
  constructor(private httpClient: HttpClient) { }

  public getSpecialOffers(): Observable<SpecialOffer[]> {
    return this.httpClient.get<SpecialOffer[]>(this.apiURL);
  }

  

  // public getSpecialOffers(specialOffer:SpecialOffer, expirationDate: string): Observable<SpecialOffer> {
  //   return this.httpClient.post<SpecialOffer>(this.apiURL + expirationDate, specialOffer, this.httpOptions);
  // }
}
