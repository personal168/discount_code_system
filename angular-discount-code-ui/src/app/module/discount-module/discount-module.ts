import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiscountCodeComponent } from './discount-code/discount-code.component';
import { RecipientComponent } from './recipient/recipient.component';
import { SpecialOfferComponent } from './special-offer/special-offer.component';
import { Routes, RouterModule } from '@angular/router';
// import { PsSelectText } from 'src/app/directive/ps-select-text.directive';

const httpRoutes: Routes = [
  { path: 'discountCode/generate', component: DiscountCodeComponent }
];

@NgModule({
  declarations: [DiscountCodeComponent, RecipientComponent, SpecialOfferComponent],
  imports: [
    CommonModule, RouterModule.forChild(httpRoutes)
  ],
  exports: [DiscountCodeComponent, RecipientComponent, SpecialOfferComponent]
})
export class DiscountModule { }
