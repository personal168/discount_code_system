import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DiscountCodeService } from 'src/app/service/discount-code/discount.code.service';
import { DiscountCode } from 'src/app/models/discountCode.model';
import { ErrorDetails } from 'src/app/models/error-details.model';

@Component({
  selector: 'app-discount-code',
  templateUrl: './discount-code.component.html',
  styleUrls: ['./discount-code.component.css'],
  providers: [DiscountCodeService]
})
export class DiscountCodeComponent implements OnInit {

  @Input() selectedOffer;
  @Input() expirationDate;
  @Input() discountCode;
  @Input() email;
  percentageDiscount: number;
  discountCodes: DiscountCode[];
  errorMsg; string;
  validDiscountsWithOfferName: Map<string, string>;
  // @Output() currentRecordEvent = new EventEmitter();

  constructor(private discountCodeService: DiscountCodeService) { }

  ngOnInit(): void {
    if (this.selectedOffer && this.expirationDate) {
      this.generateDiscountCodes();
    } else if (this.email && this.discountCode) {
      this.getPercentageDiscount();
    } else if (this.email) {
      this.getValidDiscounts();
    }
  }

  getValidDiscounts() {
    this.discountCodeService.getValidDiscountsForEmail(this.email).subscribe(
      (data: Map<string, string>) => {
        if (!this.validDiscountsWithOfferName) {
          this.validDiscountsWithOfferName = new Map<string, string>();
        }
        this.validDiscountsWithOfferName = data;
        for (const [discountCode, offerName] of Object.entries(data)) {
          console.log(discountCode, offerName);
        }
      }
    );
  }

  getPercentageDiscount() {
    this.discountCodeService.getPercentageDiscount(this.email, this.discountCode).subscribe(
      (data: number) => {
        this.percentageDiscount = data;
      }
    );
  }

  generateDiscountCodes() {
    console.log('Discount Code Component: expDate:', this.expirationDate);
    this.discountCodeService.generateDiscountCodes(this.selectedOffer, this.expirationDate).subscribe(
      (data: DiscountCode[]) => {
        if (!this.discountCodes) {
          this.discountCodes = new Array();
        }
        data.forEach(e => {
          let discountCode: DiscountCode = {
            code: e.code, canUseOnce: e.canUseOnce, expirationDate: e.expirationDate, usageDate: e.usageDate, recipient: e.recipient, specialOffer: e.specialOffer
          };
          this.discountCodes.push(discountCode);
        });
      },
      (error: ErrorDetails) => {
        this.errorMsg = error.message;
      }
    );
  }

}
