import { Component, OnInit, Input } from '@angular/core';
import { SpecialOfferService } from 'src/app/service/special-offer/special.offer.service';
import { SpecialOffer } from 'src/app/models/specialoffer.model';

@Component({
  selector: 'app-special-offer',
  templateUrl: './special-offer.component.html',
  styleUrls: ['./special-offer.component.css'],
  providers: [SpecialOfferService]
})
export class SpecialOfferComponent implements OnInit {

 

  constructor(private specialOfferService: SpecialOfferService) { }

  ngOnInit(): void {
    
  }

}
