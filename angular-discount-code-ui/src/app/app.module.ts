import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { DiscountModule } from './module/discount-module/discount-module';
import { SharedGridComponent } from './shared/shared-grid/shared-grid.component';
import { InputPageComponent } from './shared/input-page/input-page.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { DiscountCodeComponent } from './module/discount-module/discount-code/discount-code.component';
// import { PsSelectText } from './directive/ps-select-text.directive';

const httpRoutes: Routes = [
  { path: '', component: InputPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SharedGridComponent,
    InputPageComponent//,
    // PsSelectText
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DiscountModule,
    HttpClientModule,
    RouterModule.forRoot(httpRoutes),
    FormsModule,
    NgbModule
  ],
  // exports:[InputPageComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
