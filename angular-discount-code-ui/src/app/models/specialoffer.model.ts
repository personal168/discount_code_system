import { DiscountCode } from './discountCode.model';

export interface SpecialOffer {
    specialOfferId: number;
    name: string;
    fixedPercentageDiscount: number;
    discountCodes?: DiscountCode[];
}