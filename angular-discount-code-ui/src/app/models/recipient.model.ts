import { DiscountCode } from './discountCode.model';

export interface Recipient {
    recipientId: number;
    name: string;
    email: string;
    discountCodes: DiscountCode[];
}