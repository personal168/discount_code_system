export interface ErrorDetails {
    timestamp: Date;
    message: string;
    details: string;
    statusCode: number;
}