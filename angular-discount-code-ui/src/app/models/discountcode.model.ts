import { Recipient } from './recipient.model';
import { SpecialOffer } from './specialoffer.model';

export interface DiscountCode {
    code: string;
    recipient: Recipient;
    specialOffer: SpecialOffer;
    expirationDate: Date;
    canUseOnce: boolean;
    usageDate: Date;
}