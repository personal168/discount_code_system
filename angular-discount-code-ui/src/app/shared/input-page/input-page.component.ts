import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { SpecialOffer } from 'src/app/models/specialoffer.model';
import { SpecialOfferService } from 'src/app/service/special-offer/special.offer.service';

@Component({
  selector: 'app-input-page',
  templateUrl: './input-page.component.html',
  styleUrls: ['./input-page.component.css'],
  providers: [SpecialOfferService]
})
export class InputPageComponent implements OnInit {

  model: NgbDateStruct;
  date: { year: number, month: number, day: number };
  specialOffers: SpecialOffer[];
  selectedSpecialOffer: SpecialOffer;
  expDate: string;
  canSend: boolean = false;
  isToGetPercentage: boolean = false;
  discountCode: string;
  email: string;

  constructor(private specialOfferService: SpecialOfferService) {
  }

  ngOnInit(): void {
    this.getAllSpecialOffers();
  }

  getAllSpecialOffers() {
    this.specialOfferService.getSpecialOffers().subscribe(
      (data: SpecialOffer[]) => {
        if (!this.specialOffers) {
          this.specialOffers = new Array();
        }
        data.forEach(e => {
          let offer: SpecialOffer = {
            name: e.name,
            specialOfferId: e.specialOfferId,
            fixedPercentageDiscount: e.fixedPercentageDiscount,
            discountCodes: e.discountCodes
          };
          this.specialOffers.push(offer);
        });
      }
    );
  }

  generateDiscounts() {
    if (this.model.month < 10) {
      this.expDate = this.model.year + '-' + '0' + this.model.month + '-' + this.model.day;
    } else {
      this.expDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
    }
    this.canSend = true;
  }

  getSelectedOffer($event): SpecialOffer {
    let text = $event.target.options[$event.target.options.selectedIndex].text;
    console.log(text);
    this.specialOffers.forEach(offer => {
      if (offer.name == text) {
        this.selectedSpecialOffer = offer;
      }
    });
    return this.selectedSpecialOffer;
  }

  getPercentageDiscount() {
    console.log(this.discountCode);
    this.canSend = true;
  }

  getValidDiscounts() {
    this.canSend = true;
  }

}
